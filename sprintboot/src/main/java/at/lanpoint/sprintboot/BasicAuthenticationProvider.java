package at.lanpoint.sprintboot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class BasicAuthenticationProvider implements AuthenticationProvider {

	//authentication is here
	public Authentication authenticate(Authentication arg0) throws AuthenticationException {  
		System.out.println("LOGIN: " + arg0.getName() + " " + arg0.getCredentials());
        
        //TODO PSIpenta Authentication has to made here
        if(arg0.getName().equals("ben") && arg0.getCredentials() != null && arg0.getCredentials().equals("passme")) {
        	List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
            Authentication auth = new UsernamePasswordAuthenticationToken(arg0.getName(), arg0.getCredentials(), grantedAuths);
            return auth;
        }
        
		return null;
	}

	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
