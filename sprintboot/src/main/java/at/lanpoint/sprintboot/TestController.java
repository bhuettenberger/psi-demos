package at.lanpoint.sprintboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Hello world!
 *
 */
@Controller
public class TestController 
{
	@RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!";
    }
}
